from http.server import ThreadingHTTPServer, BaseHTTPRequestHandler
import time
from os import urandom
from base64 import b64encode
import threading

prefix = b64encode(urandom(12)).decode().replace("/","_")
print(f"http://localhost:9149/{prefix}/mon")

warning = """
THIS INCIDENT HAS BEEN LOGGED

DON'T TRY ACCESSING THIS WEBSERVER. IT'S NOT PART OF THE TASK

WAIT HOW DID YOU EVEN GET HERE

GO AWAY
"""

event_streams = dict()

queue_lock = threading.Lock()

def queue_event_for_all_streams(data):
	with queue_lock:
		for es in event_streams.keys():
			event_streams[es].append(data)

def announce_unauth(a):
	queue_event_for_all_streams("event: unauth\ndata: \""+a+"\"\n")

chill_paths = ["/favicon.ico"]

class QuackMonitorHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		if self.path.startswith("/" + prefix):
			path = self.path[len(prefix)+1:]
			print(path)
			if path == "/mon":
				self.send_response(200)
				self.end_headers()
				monitor_page = open("monitor.html", "r").read()
				self.wfile.write(monitor_page.encode())
			elif path == "/es":
				self.send_response(200)
				self.send_header("Cache-Control", "no-cache")
				self.send_header("Content-Type", "text/event-stream")
				self.end_headers()
				event_streams[self] = []
				try:
					while True:
						while len(event_streams[self]):
							evt = event_streams[self].pop(0) + "\n"
							self.wfile.write(evt.encode())
						self.wfile.write(b"event: mon_ping\ndata: \n\n")
						self.wfile.flush()
						time.sleep(1)
				except BrokenPipeError as bp:
					self.log_message("Event stream closed")
					del event_streams[self]
				except Exception as e:
					raise e
		else:
			if self.path not in chill_paths:
				announce_unauth(f"{self.address_string()} @ {self.path}")
			self.send_response(403)
			self.end_headers()
			self.wfile.write(warning.encode())

def thread_http_code():
	server = ThreadingHTTPServer(('127.0.0.1', 9149), QuackMonitorHandler)
	server.serve_forever()

def thread_fifo_code():
	with open("/tmp/log.fifo", "r") as flog:
		while True:
			LL = ""
			while True:
				L = flog.readline().rstrip("\n")
				if L == "":
					break
				LL += L + "\n"
			queue_event_for_all_streams(LL)

thread_http = threading.Thread(target=thread_http_code)
thread_fifo = threading.Thread(target=thread_fifo_code)

thread_http.start()
thread_fifo.start()
