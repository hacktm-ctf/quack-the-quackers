# Quack The Quackers

## Scenario

Out company was breached and multiple computers were phisically compromised. We
found this weird device supposedly called a "Digispark" and dumped its memory,
but we couldn't figure out anything else about what happened. Could you please
take a look? Also, please try to find out exactly what data the attackers
managed to exfiltrate.

Attached: quack_the_quackers.rom

## Solution overview

1. The ROM is a slightly obfuscated rubber ducky-like program which downloads
	and executes powershell code from http://nmdfthufjskdnbfwhejklacms.xyz/-ps
2. That code downloads a windows binary quack.exe, which is to be analysed. It:
	- Connects to nmdfthufjskdnbfwhejklacms.xyz:19834
	- Implements 3 kinds of packets:
		* '@' - Ping - checks connection to the server by echoing some data
		* 'L' - Listing - sends the server a list of max 255 files from the
			current directory and waits for the server to pick one
		* 'f' - File exfil - sends the first 255 bytes of the chosen file to the
			server
	- Every 10 seconds it pings the server, sends a listing, then honors the
		server's file exfil request
3. The server is to be blindly exploited. The vulnerability is inspired from
	Heartbleed and can be triggered by sending a ping packet with a certain
	payload length, but doing a shutdown("send") on the socket before fully
	transmitting the payload. As the server uses the heap to temporarily store
	client packets, this will result in a chunk being allocated, and only its
	beginning being initialised. The server will reply with more bytes than the
	client sent, thus leaking at most 255 bytes from the heap. Multiple clients
	will continuously sent the flag to the server so the heap will be sprayed
	with the flag.

## Files

* `c2_server` contains the server code (without the HTTP static serving)
* `c2_http` contains the files to be statically served on port 80 (`_ps` and
`quack.exe`)
* `victim` contains the victim executable and assembly source
* `digispark_rubber_ducky` contains the Arduino source for the initial ROM
* `public` contains only the compiled ROM
