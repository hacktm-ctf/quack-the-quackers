#include <pthread.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h> // sockaddr_in
#include <errno.h>
#include <ctype.h>	// isprint
#include <unistd.h>	// close
#include <stdlib.h> // dynamic memory
#include <stdbool.h>
#include <fcntl.h> // open
#include <string.h>

#define FLAG "HackTM{Qu4ck_m3_b4ck_b4by!}"
#define HARDCODED_FLAG_FILENAME "P32CSWFGHW5RWIV4DQ2ST5A6"

#define 

const char fallback_text[255] = "IMPORTANT COMPANY SECRET: HackTM{Qu4ck_m3_b4ck_b4by!}HAT. Lucas requests the HackTM{Qu4ck_m3_b4ck_b4by!} page. Eve (administrator) wants to set the server's master key to HackTM{Qu4ck_m3_b4ck_b4by!}. Isabel wants pages about HackTM{Qu4ck_m3_b4ck_b4by!}.zz";

// I wanted to do some public key crypto
// I ended up with this being just a xor key
char pubkey[16] = {};
// oh god

const char flag[] = FLAG;

#define INFO   "\033[32mINFO\033[0m "
#define ERROR  "\033[91mERROR\033[0m "
#define WARN   "\033[33mWARN\033[0m "
#define CLIENT "\033[36m%2d\033[0m "
#define SERVER " + "

unsigned int my_random() {
	static int dev_urandom_fd = 0;
	if(dev_urandom_fd == 0) dev_urandom_fd = open("/dev/urandom", O_RDONLY);
	int r;
	read(dev_urandom_fd, &r, 4);
	return r;
}

int fullrecv(const int sock, void *buf, int len) {
	int remaining = len;
	int done = 0;
	while(remaining) {
		int r = recv(sock, buf+done, remaining, 0);
		if(r == 0) return EOF;
		done += r;
		remaining -= r;
	}
	return done;
}

void *handle_connection(void *params) {
	const int conn = *((int*)params);
	char opcode;
	if(fullrecv(conn, &opcode, 1) == EOF) {
		fprintf(stderr, CLIENT ERROR "Connecion closed before opcode", conn);
		printf("event: status\ndata: \"EOF opcode\"\n\n");
		goto close_conn;
	}
	switch(opcode) {
		case '@': {
			fprintf(stderr, CLIENT INFO "PING\n", conn);
			
			unsigned char len;
			if(fullrecv(conn, &len, 1) == EOF) {
				fprintf(stderr, CLIENT ERROR "PING EOF len\n", conn);
				printf("event: status\ndata: \"EOF ping len\"\n\n");
				goto close_conn;
			}

			/* HEARTBLEED VULNERABLE CODE */
			char *payload = malloc(len);	// MALLOC DOES NOT INITIALISE!!!
			if(payload == 0) {
				fprintf(stderr, CLIENT ERROR "malloc(%d) failed\n", conn, len);
				printf("event: status\ndata: \"malloc failed\"\n\n");
				goto close_conn;
			}
			FALLBACK memcpy(payload, fallback_text, len);

			if(fullrecv(conn, payload, len) == EOF) {
				fprintf(stderr, CLIENT "\033[30;103m WIN \033[0m Bug triggered!\n", conn);
				printf("event: win\ndata: {\"fd\":%d}\n\n", conn);
			}

			if(send(conn, payload, len, 0) == -1) {
				if(errno == EFAULT) {
					fprintf(stderr, CLIENT ERROR "send() EFAULT\n", conn);
				} else {
					fprintf(stderr, CLIENT ERROR "send() returned -1; errno = %d\n", conn, errno);
				}
				printf("event: status\ndata: \"send() error %d\"\n\n", errno);
			}

			printf("event: ping\ndata: {\"fd\":%d,\"payload_hex\":\"", conn);
			for(int i=0; i<len; i++) printf("%02hhx", payload[i]);
			printf("\"}\n\n");

			free(payload);
			/* END HEARTBLEED */
		} break;
		case 'L': {
			// Receives a list of max 255 filenames of max 255 chars
			// Chooses one at random or, if it exists, the flag file, and sends its name back
			char chosen[256] = {};
			char any[256] = {};
			bool found_flag = false;

			unsigned char numfiles;
			if(fullrecv(conn, &numfiles, 1) == EOF) {
				fprintf(stderr, CLIENT ERROR "LISTING EOF numfiles\n", conn);
				printf("event: status\ndata: \"EOF numfiles\"\n\n");
				goto close_conn;
			}

			fprintf(stderr, CLIENT INFO "LISTING %d files\n", conn, numfiles);
			printf("event: listing\ndata: {\"fd\":%d,nfiles:%d}\n\n", conn, numfiles);

			if(numfiles == 0) {
				goto close_conn;
			}

			unsigned char chosen_index = my_random() % numfiles;

			while(numfiles--) {
				unsigned char len;
				if(fullrecv(conn, &len, 1) == EOF) {
					fprintf(stderr, CLIENT ERROR "LISTING EOF filename size\n", conn);
					printf("event: status\ndata: \"EOF filename size\"\n\n");
					goto close_conn;
				}

				char *filename = malloc(len+1);
				if(filename == 0) {
					fprintf(stderr, CLIENT ERROR "malloc(%d) failed\n", conn, len);
					printf("event: status\ndata: \"malloc failed\"\n\n");
					goto close_conn;
				}
				FALLBACK memcpy(filename, fallback_text, len+1);
				if(fullrecv(conn, filename, len) == EOF) {
					fprintf(stderr, CLIENT ERROR "LISTING EOF file name\n", conn);
					printf("event: status\ndata: \"EOF filename\"\n\n");
					free(filename);
					goto close_conn;
				}
				filename[len] = 0;

				char *filename_hex = malloc(2*len+1);
				if(filename_hex == 0) {
					fprintf(stderr, CLIENT ERROR "malloc(%d) failed\n", conn, 2*len+1);
					printf("event: status\ndata: \"malloc failed\"\n\n");
				} else {
					for(int i=0; i<len; i++) {
						sprintf(&filename_hex[2*i], "%02hhx", filename[i]);
					}
					filename_hex[len*2] = 0;

					printf("event: listing_file\ndata: {\"fd\":%d,\"filename_hex\":\"%s\"}\n\n", conn, filename_hex);
					free(filename_hex);
				}
				fprintf(stderr, CLIENT INFO "LISTING %s\n", conn, filename);
				
				if(strcasecmp(filename, HARDCODED_FLAG_FILENAME) == 0) {
					strcpy(chosen, filename);
					found_flag = true;
				}
				if(!found_flag) {
					if(filename[len-1] != '/') strcpy(any, filename);
					if(numfiles == chosen_index) {
						strcpy(chosen, filename);
						// Dont ask for directories
						if(chosen[strlen(chosen)-1] == '/') chosen[0] = 0;
					}
				}
				free(filename);
			}

			if(chosen[0] == 0) strcpy(chosen, any);
			unsigned char flen = strlen(chosen);

			char *filename_hex = malloc(2*flen+1);

			if(filename_hex == 0) {
				fprintf(stderr, CLIENT ERROR "malloc(%d) failed\n", conn, 2*flen+1);
				printf("event: status\ndata: \"malloc failed\"\n\n");
			} else {
				for(int i=0; i<flen; i++) {
					sprintf(&filename_hex[2*i], "%02hhx", chosen[i]);
				}
				filename_hex[flen*2] = 0;

				printf("event: listing_ask\ndata: {\"fd\":%d,\"filename_hex\":\"%s\"}\n\n", conn, filename_hex);
				free(filename_hex);
			}

			send(conn, &flen, 1, 0);
			send(conn, chosen, flen, 0);

			fprintf(stderr, CLIENT INFO "LISTING ASKED FOR 0x%02x:%s\n", conn, flen, chosen);
		} break;
		case 'f': {
			fprintf(stderr, CLIENT INFO "FILE EXFIL\n", conn);
			unsigned char len;
			if(fullrecv(conn, &len, 1) == -1) {
				fprintf(stderr, CLIENT ERROR "EXFIL EOF len\n", conn);
				goto close_conn;
			}

			char *file_contents = malloc(len);
			if(file_contents == 0) {
				fprintf(stderr, CLIENT ERROR "malloc(%d) failed\n", conn, len);
				printf("event: status\ndata: \"malloc failed\"\n\n");
				goto close_conn;
			}
			fullrecv(conn, file_contents, len);	// Don't care to check for EOF
			FALLBACK memcpy(file_contents, fallback_text, len);

			char data_hex[513];
			for(int i=0; i<len; i++) sprintf(&data_hex[2*i], "%02hhx", file_contents[i]);
			data_hex[2*len] = 0;

			printf("event: exfil\ndata: {\"fd\":%d,\"data\":\"%s\"}\n\n", conn, data_hex);
			fprintf(stderr, CLIENT INFO "FILE EXFIL CONTENTS: %s\n", conn, data_hex);


			// Prevent people from sending fake flags to throw off other teams
			for(int i=0; i<len; i++) {
				if(strncasecmp(file_contents+i, "HackTM{", 7) == 0 &&
					strncasecmp(file_contents+i, flag, sizeof(flag)-1) != 0) {
					fprintf(stderr, CLIENT ERROR "FAKE FLAG %s\n", conn, file_contents+i);
					printf("event: status\ndata: \"fake flag\"\n\n");
					memset(file_contents, 0, len);
					break;
				}
			}

			free(file_contents);
		} break;
		default:
			fprintf(stderr, CLIENT WARN "Invalid opcode %02hhx = '%c'\n", conn, opcode, isprint(opcode) ? opcode : 0);
			printf("event: bad_opcode\ndata: {\"fd\":%d,\"opcode\":%hhd}\n\n", conn, opcode);
	}
	close_conn:
	fprintf(stderr, CLIENT INFO "Done\n", conn);
	printf("event: disconnect\ndata: {\"fd\":%d}\n\n", conn);
	close(conn);

	fflush(stdout);
	fflush(stderr);

	return 0;
}

#define htons(x) ((((x) & 0xff00) >> 8) | ((((x) & 0xff) << 8)))

int main() {
	fprintf(stderr, "\n\
  QQQQ   UU  UU   AAAA    CCCC   KK  KK\n\
 QQ  QQ  UU  UU  AA  AA  CC  CC  KK KK \n\
 QQ  QQ  UU  UU  AAAAAA  CC      KKKK  \n\
 QQ QQQ  UU  UU  AA  AA  CC  CC  KK KK \n\
  QQQQQQ  UUUU   AA  AA   CCCC   KK  KK\n\
      QQQ                              \n\
           QUACK THE QUACKERS C2 SERVER\n\n");

	for(int i=0; i<16; i++) {
		pubkey[i] = my_random() & 0xff;
	}

	int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	int one = 1;
	setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(19834);
	addr.sin_addr.s_addr = 0;	// listen on all interfaces
	memset(addr.sin_zero, 0, sizeof(addr.sin_zero));

	bind(sock, (struct sockaddr*)&addr, sizeof(addr));
	listen(sock, 1);	// Should this be bigger than 1?????

	fprintf(stderr, SERVER INFO "Bound and listening\n");

	fflush(stderr);

	while(1) {
		struct sockaddr_in client_addr;
		unsigned int alen = sizeof(client_addr);
		int conn = accept(sock, (struct sockaddr*)&client_addr, &alen);
		if(conn == -1) {
			fprintf(stderr, SERVER ERROR "accept() returned -1; errno = %d\n", errno);
			printf("event: status\ndata: \"FATAL ERROR accept() RETURNED -1; errno = %d\"\n\n", errno);
			break;
		}
		if(client_addr.sin_family != AF_INET) {
			fprintf(stderr, SERVER INFO "Dropped connection: client_addr.sin_family != AF_INET\n");
			printf("event: status\ndata: \"Dropped connection\"\n\n");
			close(conn);
			continue;
		}

		int a = client_addr.sin_addr.s_addr;
		fprintf(stderr, SERVER INFO "Connection from %d.%d.%d.%d (fd = %d)\n", a&255, (a>>8)&255, (a>>16)&255, (a>>24)&255, conn);
		printf("event: connect\ndata: {\"ip\":\"%d.%d.%d.%d\",\"fd\":%d}\n\n", a&255, (a>>8)&255, (a>>16)&255, (a>>24)&255, conn);

		pthread_t thread;
		pthread_create(&thread, 0, handle_connection, &conn);
		pthread_detach(thread);
	}
}
